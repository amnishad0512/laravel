<h1>user List</h1>
<table border="2">
    <tr>
        <td>ID</td>
        <td>Name</td>
        <td>Email</td>
        <td>Profile Photo</td>
    </tr>
@foreach ($collection as $item)
<tr>
    <td>{{$item['id']}}</td>
    <td>{{$item['first_name']}}</td>
    <td>{{$item['email']}}</td>
    <td><img src="{{$item['avatar']}}" alt="Profile"></td>
</tr>
@endforeach

</table>
