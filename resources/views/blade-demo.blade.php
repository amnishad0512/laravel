{{-- Access data from array --}}
{{-- <h1> Hello, {{$name[0]}} how are you?</h1> --}}

{{-- use count method of array --}}
{{-- <h1> In array(name) total value are {{count($name)}} </h1> --}}

{{-- Core PHP syntax --}}
{{-- <h1> Hello, @php
    echo $name[1];
@endphp how are you?</h1> --}}

{{-- if else --}}
{{-- @if ($name[1] == 'Prince')
<h1>Yes</h1>
@else
<h1>No</h1>
@endif --}}

{{-- For-loop --}}
{{-- @for ($i = 0;$i<count($name);$i++)
  {{"Hello ".$name[$i]}}<br>
@endfor --}}

{{-- for each --}}
{{-- @foreach ($name as $user )
{{$user}}
@endforeach --}}


{{-- php in js --}}
{{-- <script>
var arr = @json($name);
for(var i =0; i<arr.length;i++){
 console.log("hello",arr[i]);
}
// </script> --}}


{{--
                @php
 {{10+30}} ==       echo 10+30;
                @endphp
--}}
