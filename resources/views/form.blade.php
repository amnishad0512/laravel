<h1>User login</h1>
{{-- @if ($errors->any())
@foreach ($errors->all() as $err )
<li>{{$err}}</li>
@endforeach
@endif --}}

<form action="data" method="POST">
@csrf
    <input style="margin-bottom: 5px" type="text" name="username" placeholder="Enter username"><br>
    <span style="color: red">@error('username'){{$message}}@enderror</span><br>
    <input style="margin-bottom: 5px" type="password" name="password" placeholder="Enter password"><br>
     <span style="color: red">@error('password'){{$message}}@enderror</span><br>
    <button type="submit">Login</button>
</form>

{{-- use for show error --}}
{{-- {{$errors}} --}}
