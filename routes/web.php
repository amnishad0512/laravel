<?php

//!import controller to access it
use App\Http\Controllers\UserAuth;
use Illuminate\Support\Facades\Route;

//! Routing
//define - make new url for project page
// ? default page
// Route::get('/', function () {
//     return view('default');
// });

//! View
//? Method 1
// Route::get('/user', function () {
//     return view('user');
// });

//? Method 2
// Route::view('/user', 'user');

//? to fetch dynamic value from url
// Route::get('/hello/{name}', function ($name) {
//     echo $name;
//     return view('hello');
// });

//? to fetch dynamic value from url and pass to hello as a array
// Route::get('/hello/{name}', function ($name) {
//     return view('hello',['name'=>$name]);
// });

//! Controller
// Define - It is a central that use for connect model(Database) to view(Html Part) And vice-versa;
// create - php artisan make:controller _____
// import - use App\Http\Controllers\_______

//? simple call
// Route::get('user',[UserController::class,'show']);

//? to fetch dynamic value from url and pass to hello as a array
// Route::get('user/{name}', [UserController::class, 'show']);

//! Component
// Define - piece of code to reuse
// Create - php artisan make:component ____
//? Access header.php && header.blade.php - Data pass in component

// Route::get('/user', function () {
//     return view('user');
// });
// Route::get('/about', function () {
//     return view('about');
// });

//! Middleware
// Define - To create specific set of rules or condition;
//? Type
//1. Global - apply for all pages
//2. Grouped - when apply specific page of all of them
//3. Routed - When apply for single route
// create - php artisan make:middleware checkAge
// add - before use must register in kernel

//? For global Middleware
// Route::view('/', 'welcome');
// Route::view('noAccess', 'noAccess');
// Route::view('user', 'user');

//? For group Middleware
// Route::group(['middleware' => ['protectPage']], function () {
//     Route::view('/user', 'user');
// });

//? For Route Middleware
// Route::view('/', 'welcome');
// Route::view('noAccess', 'noAccess')->middleware('protectedPage');
// Route::view('user', 'user')->middleware('protectedPage');

//! Blade template
// Define -  To make the simplify code of PHP
// Route::view('/demo', 'blade-demo');
// Route::get('/blade',[UserController::class,'loadView']);

//! anchor tag use
// Route::view('/hello', 'hello');
// Route::view('/contact','contact');
// Route::view('/about','about');

//! redirect page to another page
// Route::get('/', function () {
//     return redirect('about');
// });

//! HTML Form and Validation
// Route::post('data',[UserController::class,'getData']);
// Route::view('login','form');

//! HTTP Client (API)
// Define - To consume API in laravel
// Make Controller
//  Route::get('api',[UserController::class,'index']);

//! HTTP Request
//? Method
// GET - get data or show
// POST - save
// PUT - update
// DELETE -  delete
// PATCH
// OPTIONS
// Route::post('submit',[UserController::class,'testReq']);
// Route::view('contact','contact');

! Session
Route::view('login', 'login');
Route::view('profile', 'profile');

//? for logout
Route::get('logout', function () {
    if (session()->has('user')) {
        session()->pull('user', null);
    }
    return redirect('login');
});

//? for login
Route::get('login', function () {
    if (session()->has('user')) {
        return redirect ('profile');
    }
    return view('login');
});
Route::post('user', [UserAuth::class, 'userLogin']);

//!Flash Session
