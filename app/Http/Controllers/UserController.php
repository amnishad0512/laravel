<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

//? It import for API call
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    public function show($name)
    {
        //?simple return string
        return "hello, I am user Controller";

        //? return view
        return view('hello');

        //? use for dynamic data
        return $name;
    }
    public function loadView()
    {
        //? send data to view in array
        return view('blade-demo', ['name' => ['Manoj', 'Prince', 'Rahul', 'Rohinee', 'shweta']]);
    }
    function getData(Request $rq)
    {
        //? validation
        $rq->validate([
            'username'=>'required | max:10',
            'password'=>'required | min:7'

        ]);
        return $rq->input();
        //  return "Your Form has Submitted Successfully :)";
    }
    function index(){
        // echo "API call will be here";
        $data =  Http::get("https://reqres.in/api/users?page=2");
        return view('api',['collection'=>$data['data']]);
    }
    function testReq(Request $req){

        return $req->input();
    }
}
